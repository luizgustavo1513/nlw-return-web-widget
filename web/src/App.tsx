import { Widget } from "./Components/Widget" //capta o widget feito dentro da pasta components
//Componente, sempre com letra maiuscúla e é basicamente qualquer função que retorne HTML
//Propriedades. Não se pode ter vários componentes retornando sem estarem "envoltos em um componente maior"
export function App() { 
  return <Widget /> // retorna o widget pego ao main.tsx que renderiza isso no index.html
}