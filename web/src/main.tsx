import React from 'react'
import ReactDOM from 'react-dom/client'
import {App} from './App' //importa o arquivo de nomw App na pasta

import './Global.css' //faz import das configs dentro do Global.css


//cria no index as linhas captadas pelo app
ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
)
