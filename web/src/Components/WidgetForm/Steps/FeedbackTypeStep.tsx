import { feedbackTypes, FeedbackType } from ".."
import { CloseButton } from "../../CloseButton";


//interface nomeDoComponente+Props serve para receber as funções passadas como propriedades do componente pai e utilizar elas aqui no componente filho

interface FeedbackTypeStepProps{
    onFeedbackTypeChanged: (type:FeedbackType)=> void;
}

/*
    Object.entries(objetoEmQuestão) vetoriza os objetos do objeto, ou seja, tranforma numa lista de listas [[ele1,ele2,...],[],[],...]
    .map((key,value)=>{}) percorre o vetor por completo e como a lista só tem 2 valores por objeto vetorizado um deles será chamado chave e o outro será o valor dessa chave pra função chamada
*/
//Key precisa ser setada para n dar erro

//para passar a função por um evento (onClick) você precisa passar a função e não sua execução, por exemplo quando eu passo onClick={onFeedbackTypeChanged} eu passo a função mas não a sua execução, se eu passo onClick={onFeedbackTypeChanged()} eu passo a execução da função. A maneira correta de fazer isso é por Arrow Function pois assim você passa a execução da função que só vai ser realizada quando o botão for apertado

//nos parametros dessa função nós passamos as funções importadas do componente pai para utilizarmos e desestruturamos ela ao passar no meio de chaves

export function FeedbackTypeStep({onFeedbackTypeChanged} : FeedbackTypeStepProps){
    return(
        <>
        <header>
                <span className="text-xl leading-6 flex items-center gap-2">
                    Deixe Seu feedback
                </span>

                <CloseButton/>
        </header>
        <div className="flex py-8 gap-2 w-full">
            { Object.entries(feedbackTypes).map(([key,value])=> {return(
                <button
                key={key}
                className="bg-zinc-800 rounded-lg py-5 w-24 flex-1 flex flex-col items-center gap-2 border-2 border-transparent hover:border-brand-500 focus:border-brand-500 focus:outline-none"
                onClick={()=>onFeedbackTypeChanged(key as FeedbackType)}
                type="button"
                >
                    <img src={value.image.source} alt={value.image.alt}/>
                    <span>{value.title}</span>
                </button>
            )})}

        </div>
        </>
        )
}