import { ArrowLeft } from "phosphor-react"
import { FormEvent, useState } from "react"
import { FeedbackType, feedbackTypes } from '..'
import { api } from "../../../lib/api";
import { CloseButton } from "../../CloseButton"
import { Loading } from "../../Loading";
import { ScreenshotButton } from "../ScreenshotButton";

//interface recebe o tipo de feedback que ele pega do componente FeedbackTypeStep

interface FeedbackContentStepProps{
    feedbackType: FeedbackType;
    onFeedbackRestartRequested: () => void;
    onFeedbackSent: () => void;
}

export function FeedbackContentStep(
    {feedbackType, onFeedbackRestartRequested, onFeedbackSent}: FeedbackContentStepProps)
    {
        //uma função pega a print e a outra o comentário
        const [screenshot,setScreenshot] = useState<string|null>(null);
        const [comment,setComment] = useState("");
        const [isSendingFeedback, setIsSendingFeedback] = useState(false);

        //pega o tipo de feedback escolhido pela pessa dentro das opções disponíveis no feedbackTypes
        const feedbackTypeInfo = feedbackTypes[feedbackType];

        //precisamos informar o formato do evento para passar para  a função 
        async function handleSubmitFeedback(event: FormEvent){
            event.preventDefault();
            setIsSendingFeedback(true);
            await api.post('/feedbacks', { type: feedbackType, comment, screenshot })
            setIsSendingFeedback(false);
            onFeedbackSent()
        }
    return(
        <>
            <header>
                <button type="button" 
                className="top-5 left-5 absolute text-zinc-400 hover:text-zinc-100"
                onClick={onFeedbackRestartRequested}
                >
                    <ArrowLeft weight="bold" className="w-4 h-4"/>
                </button>

                <span className="text-xl leading-6 flex items-center gap-2">
                    <img src={feedbackTypeInfo.image.source} alt={feedbackTypeInfo.image.alt} className="w-6 h-6" />
                    {feedbackTypeInfo.title}
                </span>

                <CloseButton/>

            </header>
            <form onSubmit={handleSubmitFeedback} className="my-4 w-full">
                
                <textarea className="min-w-[304px] w-full min-h-[112px] text-sm placeholder-zinc-400 text-zinc-100 border-zinc-600 bg-transparent rounded-md focus:border-brand-500 focus:ring-brand-500 focus:ring-1 resize-none focus:outline-none scrollbar-thumb-zinc-700 scrollbar-track-transparent scrollbar-thin" 
                placeholder="Conte com detalhes o que está acontecendo..."
                //toda vez que escrevermos algo na textarea atualizamos o valor do setComent com o valor escrito
                onChange={event=>setComment(event.target.value)}/>
                
                <footer className="flex gap-2 mt-2">
                    <ScreenshotButton
                    screenshot = {screenshot}
                    onScreenshotTook = {setScreenshot}
                    />
                

                    <button 
                    type="submit"
                    disabled={comment.length === 0 || isSendingFeedback } //propriedade para desabilitar o botão quando n tiver nenhum comentário. Também faz com que ele seja customizável para quando desabilitado
                    className="p-2 bg-brand-500 rounded-md border-transparent flex-1 flex justify-center items-center text-sm hover:bg-brand-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-zinc-900 focus:ring-brand-500 transition-colors disabled:opacity-50 disabled:hover:bg-brand-500"
                    >
                        { isSendingFeedback? <Loading/> : 'Enviar feedback' }
                    </button>
                </footer>
            </form>
        </>
    )
    //fazer novos componentes como o screenshot button faz sentido quando a funcionalidade desse componente não é relacionada ao restante da funcionalidade de um componente. Ex: nada nesse componente depende de tirar um print da tela, logo o botão de tirar um print da tela pode ser separado em um componente próprio
}