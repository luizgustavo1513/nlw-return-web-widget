//Exemplo aula 1


// import { ChatTeardropDots} from 'phosphor-react'; //biblioteca de imagem do ícone de chat
// import { Popover } from '@headlessui/react' //biblioteca de acessibilidade que faz a funcionalidade de useState


// export function Widget(){
//     return (
//         //Popover substitui div e checa useState com acessibilidade, fazendo do panel o que surge e desaparece de acordo com o Button
//         //className é a classe normal do CSS mas n pode ser usado a palavra class por ser uma palavra reservada do JS
//         //Dentro do className estão elementos do CSS escritos de maneira tailWind para agilizar a programação
//         //ChatTeardropDots é o icon importado da phosphor
//         <Popover className='absolute bottom-4 right-4'> 
//             <Popover.Panel>Hello World</Popover.Panel>
//             <Popover.Button className='bg-brand-500 rounded-full px-3 h-12 text-white flex items-center group'>
//                 <ChatTeardropDots className=' w-6 h-6'/>
//                 <span className='max-w-0 overflow-hidden group-hover:max-w-xs transition-all duration-500 ease-linear'>
//                     <span className='pl-2'></span>
//                         Feedback
//                 </span>
//             </Popover.Button>
//         </Popover>
//         //overflow-hidden esconde o excesso do elemento que aparece 
//     )
// }

//Aula 2


//Import de images
import bugImageUrl from '../../assets/bug.svg'
import ideaImageUrl from '../../assets/idea.svg'
import thoughtImageUrl from '../../assets/thought.svg'

import { useState } from "react"
import { FeedbackSuccessStep } from './Steps/FeedbackSuccessStep'
import { FeedbackTypeStep } from './Steps/FeedbackTypeStep'
import { FeedbackContentStep } from './Steps/FeedbackContentStep'

export const feedbackTypes = {
    BUG:{
        title: 'Problema',
        image: {
            source: bugImageUrl,
            alt: 'Imagem de um inseto'
        }
    },
    IDEA:{
        title: 'Ideia',
        image: {
            source: ideaImageUrl,
            alt: 'Imagem de uma lâmpada'
        }
    },
    OTHER:{
        title: 'Outro',
        image: {
            source: thoughtImageUrl,
            alt: 'Imagem de uma nuvem'
        }
    }
}

//faz com que FeedbackType só possa ser as chaves do tipo do objeto feedbackTypes
export type FeedbackType = keyof typeof feedbackTypes;

export function WidgetForm(){
    //função para receber as informações dadas pelo usuários (setFEedbackType) na variável feedbackType onde ele só pode utilizar FeedbackType ou nulo
    //não tem como dar export nessa função diretamente para mandar para um componente filho, então nós precisamos passar ela como propriedade quando incluimos o filho no pai
    const [feedbackType, setFeedbackType] = useState<FeedbackType | null>(null)
    const [feedbackSent, setFeedbackSent] = useState(false);
    //primeira função define o tipo de feedback a ser enviado
    //segunda função define se foi enviado feedback ou não

    function handleRestartFeedback(){
        setFeedbackSent(false);
        setFeedbackType(null);
    }

    return(
        //div que faz a estilização e controle do botão e do formulário de feedback
        //dentro dos operadores ternários nós chamamos outros componentes e passamos informações para eles realizarem operações dentro deles com as informações desse "componente pai". 
        <div className='bg-zinc-900 p-4 relative rounded-2xl mb-4 flex flex-col items-center shadow-lg w-[calc(100vw-2rem)] md:w-auto'> 
            { feedbackSent ? (
                <FeedbackSuccessStep onFeedbackRestartRequested = {handleRestartFeedback}/>
                ):(
                    <>
                        {!feedbackType? (
                            <FeedbackTypeStep onFeedbackTypeChanged = {setFeedbackType}/>
                        ):(
                            <FeedbackContentStep
                                feedbackType={feedbackType}
                                onFeedbackRestartRequested={handleRestartFeedback}
                                onFeedbackSent={()=> setFeedbackSent(true)}
                            />
                        )}
                    </>
            )}
            <footer className='text-xs text-neutral-400'> Feito na raça por <a href="https://twitter.com/luiz09815" className='underline underline-offset-2' target="_blank">Luiz Gustavo</a> </footer>
        </div>
    )
}