import { ChatTeardropDots} from 'phosphor-react'; //biblioteca de imagem do ícone de chat
import { Popover } from '@headlessui/react' //biblioteca de acessibilidade que faz a funcionalidade de useState
import { WidgetForm } from './WidgetForm'; //exporta a pasta index quando não especificado se ela existir


export function Widget(){
    return (
        //Popover substitui div e checa useState com acessibilidade, fazendo do panel o que surge e desaparece de acordo com o Button
        //className é a classe normal do CSS mas n pode ser usado a palavra class por ser uma palavra reservada do JS
        //Dentro do className estão elementos do CSS escritos de maneira tailWind para agilizar a programação
        //ChatTeardropDots é o icon importado da phosphor
        <Popover className='absolute bottom-4 right-4 md:bottom-8 md:right-8 flex flex-col items-end'> 
            <Popover.Panel>
                <WidgetForm/>
            </Popover.Panel>
            <Popover.Button className='bg-brand-500 rounded-full px-3 h-12 text-white flex items-center group'>
                <ChatTeardropDots className=' w-6 h-6'/>
                <span className='max-w-0 overflow-hidden group-hover:max-w-xs transition-all duration-500 ease-linear'>
                    <span className='pl-2'></span>
                        Feedback
                </span>
            </Popover.Button>
        </Popover>
        //overflow-hidden esconde o excesso do elemento que aparece 
    )
}
