module.exports = {
  content: ["./src/**/*.tsx"],
  theme: {
    extend: {
      colors: {
        brand: {
          300: "#996DFF", //cria cores novas personalizadas por hexadecimal
          500: "#8257e6",
        }
      },
      borderRadius: {
        md: "4px" //sobrescreve o tamanho médio para 4px
      }
    },
  },
  plugins: [require('@tailwindcss/forms'), //exportar plugins para usar na aplicação
            require('tailwind-scrollbar'),],
}
