import React from 'react';
import { View, Text, Linking } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

import { styles } from './styles';

export function Copyright() {
  return (
    <View>
      <Text style={styles.text}>
        Feito na raça por 
        <TouchableOpacity onPress={()=>Linking.openURL('https://gitlab.com/luizgustavo1513')}>
          <Text style={styles.link}> mim </Text>
        </TouchableOpacity>
      </Text>
    </View>
  );
}