import { StyleSheet } from 'react-native';
import { theme } from '../../theme';

export const styles = StyleSheet.create({
  text: {
    fontSize: 12,
    color: theme.colors.text_secondary,
    fontFamily: theme.fonts.medium,
  },
  link: {
    fontSize: 12,
    color: theme.colors.text_on_brand_color,
    fontFamily: theme.fonts.medium,
    borderColor: theme.colors.text_on_brand_color,
    borderWidth: 1
  }
});