import { ArrowLeft } from 'phosphor-react-native';
import React, { useState } from 'react';
import { View, TextInput, Image, Text, TouchableOpacity } from 'react-native';
import { theme } from '../../theme';
import { FeedbackType } from '../Widget';
import { styles } from './styles';
import { captureScreen } from 'react-native-view-shot';
import * as FileSystem from 'expo-file-system'

import { feedbackTypes } from '../../utils/feedbackTypes'
import { ScreenshotButton } from '../ScreenshotButton'
import { Button } from '../Button'
import { api } from '../../libs/api';

interface Props{
    feedbackType: FeedbackType,
    onFeedbackRestart: () => void,
    onFeedbackSent: () => void

}

export function Form({ feedbackType, onFeedbackRestart, onFeedbackSent}: Props) {
    const [isSendingFeedback, setIsSendingFeedback] = useState(false)
    const [screenshot, setScreenshot] = useState<string|null>(null);
    const [comment, setComment] = useState("");
    const feedbackInfo = feedbackTypes[feedbackType];

    function handleScreenshot(){
        captureScreen({
            format: 'jpg',
            quality: 0.8
        })
        .then( uri => setScreenshot(uri))
        .catch(error => console.log(error))
    }

    function removeScreenshot(){
        setScreenshot(null)
    }

    async function handleSendFeedback() {
        if(isSendingFeedback){
            return;
        }

        setIsSendingFeedback(true)
        const screenshotbase64 = screenshot && await FileSystem.readAsStringAsync(screenshot, {encoding:'base64'});

        try{
            await api.post('/feedbacks', {
                type: feedbackType,
                screenshot: `data:image/png;base64, ${screenshotbase64}`,
                comment
            })
            onFeedbackSent()
        }
        catch(e){
            console.log(e)
            setIsSendingFeedback(false)
        }
    }

  return (
    <View style={styles.container}>

        <View style={styles.header}>
            <TouchableOpacity onPress={onFeedbackRestart}>
                <ArrowLeft
                size={24}
                weight='bold'
                color={theme.colors.text_secondary}
                />
            </TouchableOpacity>

        

        <View style={styles.titleContainer}>

            <Image source={feedbackInfo.image} style={styles.image}/>
            <Text style={styles.titleText}>
                {feedbackInfo.title}
            </Text>
        </View>
        </View>

        <TextInput
        multiline
        style={styles.input}
        placeholder='Descreva o que está acontecendo em detalhes...'
        placeholderTextColor={theme.colors.text_secondary}
        autoCorrect={false}
        onChangeText={setComment}
        />

        <View style={styles.footer}>
            <ScreenshotButton
            screenshot={screenshot}
            onRemoveShot={removeScreenshot}
            onTakeShot={handleScreenshot}
            />
            <Button
            onPress={handleSendFeedback}
            isLoading={isSendingFeedback}
            />
        </View>
    </View>
  );
}