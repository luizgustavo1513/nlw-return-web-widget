import { MailAdapter, SendMailData } from "../mailadapter";
import nodemailer from "nodemailer"

const transport = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "17ef93857ac19a",
      pass: "e8c3e50958a37c"
    }
});

export class nodeMailerMailAdapter implements MailAdapter{
    async sendmail({subject, body}: SendMailData){
        //envia email
    await transport.sendMail({
        from: 'Equipe Feedget <oi@feedget.com>',
        to: 'Luiz Gustavo <luizgustavo1513@gmail.com>',
        subject,
        html: body 
    });
    };
}