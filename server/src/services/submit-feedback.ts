import { MailAdapter } from "../adapters/mailadapter";
import { FeedbackRepository } from "../repositories/feedbacks-repository";

interface submitFeedbackRequest{
    type: string;
    comment: string;
    screenshot?: string;
} //se refaz a interface pois são camadas diferentes da aplicação, aqui é camada de uso de dados lá é a camada de banco

export class submitFeedback{

    constructor(
        private feedbacksRepository:FeedbackRepository,
        private mailadapter: MailAdapter
    ){}

    async execute(req: submitFeedbackRequest){
        const { type, comment, screenshot } = req;

        if (!type || !comment){
            throw new Error('Type is required.')
        }

        if (screenshot && !screenshot.startsWith('data:image/png;base64')){
            throw new Error('Invalid Screenshot format.')
        }

        //SEM PRINCIPIO DE INVERSÃO DE DEPENDENCIA
        // Desse jeito fica complicado alterar o método do prisma em algum momento do futuro
        // const prismaFeedbacksRepository = new PrismaFeedbacksRepository();

        // await prismaFeedbacksRepository.create({
        //     type,
        //     comment,
        //     screenshot
        // });

        //se um dia eu quiser trocar o repositório do Prisma eu posso pois nenhuma classe nesse repositório depende dele

        await this.feedbacksRepository.create({
            type,
            comment,
            screenshot
        });

        await this.mailadapter.sendmail({
            subject: "Novo Feedback",
            body: [
                    `<div style="font-family: sans-serif; font-size:16px; font-color: #111;">`,
                    `<p>Tipo do Feedback: ${type}</p>`,
                    `<p>Comentário do Feedback: ${comment}</p>`,
                    screenshot ? `<img style=" width: 256px" src="${screenshot}"/>` : null,
                    `</div>`
                ].join('\n')
        })

    }
}