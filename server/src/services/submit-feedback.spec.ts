//testa como funciona uma parte do código em separado do resto, ou seja, ele testa se ele entrega o que é esperado mas não a interação da função com o resto da aplicação

//responsabiliade do teste unitário é testar o conteúdo da função apenas 

import { submitFeedback } from "./submit-feedback"

//spies = maneiras de saber se as funções foram chamadas 

const FeedbackSpy = jest.fn();
const MailSpy = jest.fn()

const SubmitFeedback = new submitFeedback(
    { create: FeedbackSpy },
    { sendmail: MailSpy }
)

describe('submitFeedback',()=>{
    it('Should be able to submit a feedback',
     async () =>{
         
         await expect(SubmitFeedback.execute({
             type: 'a',
             comment:"a",
             screenshot:"data:image/png;base64,ajdkjsdkasjd"
         })).resolves.not.toThrow(); //espera que a função chege até o fim e não dispare erro

         expect(FeedbackSpy).toHaveBeenCalled();
         expect(MailSpy).toHaveBeenCalled();
     })

     it('Should not be able to submit a feedback without type',
     async () =>{
         
         await expect(SubmitFeedback.execute({
             type: '',
             comment:"a",
             screenshot:"data:image/png;base64,ajdkjsdkasjd"
         })).resolves.toThrow(); //espera que a função chege até o fim e dispare erro
     })

     it('Should not be able to submit a feedback without correct screenshot format',
     async () =>{
         
         await expect(SubmitFeedback.execute({
             type: 'asda',
             comment:"a",
             screenshot:"data:image/png,ajdkjsdkasjd"
         })).resolves.toThrow(); //espera que a função chege até o fim e dispare erro
     })
})