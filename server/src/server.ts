import express from "express";
import cors from "cors";
import { routes } from "./routes";

const app = express();

app.use(express.json()) //para usar arquivos json
app.use(cors()) //cors serve para definir quais endereços de front-end podem acessar o back-end  | do jeito que tá tá aberto
app.use(routes);

//GET = PEGAR INFO
//POST = FAZER INPUT DE INFO
//PUT = MODIFICAR INFOS DO BANCO 
//PATCH = MODIFICAR UMA INFO ÚNICA
//DELETE

/*Principios SOLID

S => Single Responsibility Principle

Cada classe/função tem uma responsabilidade única


O => Open/Closed Principle

As classes da aplicação devem ser abertas para extensão mas fechadas para modificação. Ex: Se uma classe faz algo uma segunda classe pode vir a extender seu funcionamento mas não modificá-lo


L => Liskov Substitution Principle

Determina que nós devemos poder substituir uma classe pai por uma herança dela e tudo deve continuar funcionando


I => Interface Segregation Principle

Tentar separar o máximo possível as interfaces


D => Dependency Inversion Principle

Quando a classe depende de algo externo nós devemos passar externamente para ela o que ela precisa 
*/

//Sincroniza com o mailtrap para testar envio de email

app.listen(3333, () => {console.log('HTTP server running!')})