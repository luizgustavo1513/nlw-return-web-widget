import express from "express";
import { submitFeedback } from "./services/submit-feedback";
import { PrismaFeedbacksRepository } from "./repositories/prisma/prisma-feedback-repository";
import { nodeMailerMailAdapter } from "./adapters/nodemailer/nodemailermailadapter";

export const routes  = express.Router();

routes.post('/feedbacks', async (req,res) => {
    const {type,comment,screenshot} = req.body;

    const prismaFeedbacksRepository = new PrismaFeedbacksRepository();
    const NodeMailerMailAdapter = new nodeMailerMailAdapter();

    const SubmitFeedback = new submitFeedback(prismaFeedbacksRepository, NodeMailerMailAdapter) //segue a ordem que está no constructor da aplicação

    await SubmitFeedback.execute({
      type,
      comment,
      screenshot
    })

    return res.status(201).send();
})