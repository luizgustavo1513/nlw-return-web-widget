import { prisma } from "../../prisma";
import { FeedbackCreateData, FeedbackRepository } from "../feedbacks-repository";

//Fazendo isso nós separamos a implementação das operações para que, se no futuro queiramos trocar o prisma-feedback-repositories possamos fazer isso mais facilmente criando outra classe 
export class PrismaFeedbacksRepository implements FeedbackRepository{
    async create({type, comment, screenshot}: FeedbackCreateData) {
        await prisma.feedback.create({
            data: {
                type: type, //quando o dado for igual aos dois lados do : pode ser feito da seguinte maneira
                comment,
                screenshot,
            }
        });
    }
}